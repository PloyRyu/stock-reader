import shelve
import pandas as pd
# python -mpip install matplotlib
import matplotlib.pyplot as plt
import db_params
from sqlalchemy import create_engine
pd.set_option('display.max_rows', 600)
pd.set_option('display.max_columns', 600)
pd.set_option('display.width', 1000)
def graphPlot(gformat):
    try:
        engine = create_engine('postgresql://{0}:{1}@{2}:{3}/{4}'.format(db_params.user,
                                                                         db_params.password,
                                                                         db_params.host,
                                                                         db_params.port,
                                                                         db_params.database))

        if engine.dialect.has_table(engine, 'year_stock'):
            sql = """SELECT date
                            ,high
                            ,low
                            ,open
                            ,close
                    FROM year_stock
                        """
            df = pd.read_sql_query(sql, con=engine)
            df = df.set_index('date')
            print(df)

            # Plot
            df_ts = df.resample(gformat).max()
            df_ts.plot(figsize=(12, 8), stacked=True)
            plt.show()
        else:
            print("No data - Please do full load")
    except Exception as error:
        print(error)


if __name__ == '__main__':
    # Daily
    # graphPlot('D') #Please uncomment this line for running Daily graph
    # Monthly
    # graphPlot('W') #Please uncomment this line for running Weekly graph
    # Weekly
    graphPlot('M') #Please uncomment this line for running Monthly graph

