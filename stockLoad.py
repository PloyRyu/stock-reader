# Install yfinance package.
import yfinance as yf
import pandas as pd
import datetime
import db_params
from sqlalchemy import create_engine
import timeit
from datetime import timedelta
import psycopg2
pd.set_option('display.max_rows', 600)
pd.set_option('display.max_columns', 600)
pd.set_option('display.width', 1000)
# pip install yfinance



def fullLoad(stock_name,d,m,y):
    start_time = timeit.default_timer()
    todaydate = datetime.date(y, m, d)
    print ("FULL todaydate: ",todaydate)
    startdate = datetime.date(y-1, m, d)
    print ("FULL startdate: ",startdate)
    last_date = datetime.date(y, m, d+1)
    # Get data by specifying the stock ticker, start date, and end date
    data = yf.download(stock_name, start = startdate, end = todaydate)
    data=data.reset_index()
    data["stock_name"] = stock_name
    data.columns = ["date", "high", "low", "open", "close", "adj_close", "volume", "stock_name"]

    '''
    can also use "period" instead of start/end
    valid periods: 1d,5d,1mo,3mo,6mo,1y,2y,5y,10y,ytd,max
    data = yf.download(stocks, period = '1y')
    '''

    engine = create_engine('postgresql://{0}:{1}@{2}:{3}/{4}'.format(db_params.user,
                                                                     db_params.password,
                                                                     db_params.host,
                                                                     db_params.port,
                                                                     db_params.database))
    table_name = "year_stock"
    data.to_sql(con=engine,schema='public', name=table_name, if_exists='replace', index=False)

    print("Insert full load data to DB {0}s".format(timeit.default_timer() - start_time))
def deltaLoad(stock_name,d,m,y):
    try:
        todaydate = datetime.date(y, m, d)
        print("DELTA todaydate",todaydate)
        # Get the data for the stock by specifying the stock ticker, start date, and end date
        stocks = stock_name
        start_time = timeit.default_timer()
        engine = create_engine('postgresql://{0}:{1}@{2}:{3}/{4}'.format(db_params.user,
                                                                         db_params.password,
                                                                         db_params.host,
                                                                         db_params.port,
                                                                         db_params.database))

        # check if full load has been done
        if engine.dialect.has_table(engine, 'year_stock'):
            # get max date
            sql = """SELECT max(date) as max_date
                            FROM year_stock
                    """
            df = pd.read_sql_query(sql,con=engine)
            startdate = df.at[0, "max_date"]
            # plus 2 date due to yf.download will get data since date -1
            startdate = startdate.date()+ timedelta(days=2)
            print("DELTA startdate", startdate)
            if startdate < todaydate :
                data = yf.download(stocks, start=startdate, end=todaydate)
                data = data.reset_index()
                data["stock_name"] = stock_name
                # specify column & order
                data.columns = ["date", "high", "low", "open", "close", "adj_close", "volume", "stock_name"]
                table_name = "year_stock"
                data.to_sql(con=engine, schema='public', name=table_name, if_exists='append', index=False)
                print("Insert delta load data to DB: {0}s".format(timeit.default_timer() - start_time))
            else:
                print("Your data is up to date")
        else:
            print("Please do full load before delta load")
    except Exception as error:
        print(error)
if __name__ == '__main__':
    stock_name = 'LH'
    # Input parameter is current date
    fullLoad(stock_name,20,5,2020) # Please uncomment this line for running full load
    deltaLoad(stock_name,30,5,2020) # Please uncomment this line for running full load