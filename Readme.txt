This folder contain 2 script 
Plese see main part on the bottom of both file. 

- stockLoad.py
	User can choose ticker(stock name) ie. TRUE, LH,...
    or you can use this python library to get all ticker https://pypi.org/project/Yahoo-ticker-downloader/
    User can choose to run full or incremental(delta) load.
	# Input parameter is current date for that load
	for example full load is run on 18/09/19  and run delta load on 20/09/19
	the input will look like this
    fullLoad(18,9,2019)
    deltaLoad(20,9,2019) 
    **There must be full load before delta load**

- graphPlot.py
	User can choose to display graph Daily, Monthly or Weekly by passing input parameter.
	# Daily
    graphPlot('D') 
    # Monthly
    graphPlot('W') 
    # Weekly
    graphPlot('M')

  Unsatisfied requirement
  - graph shows average and P/E ratio